const xmlescape = require("xml-escape");

chrome.runtime.onMessage.addListener(
  function(request, sender, onSuccess) {
    if (request.msg === "fetchBookmarkFromUrl") {
      fetch(request.url)
        .then(response => {
          return response.json();
        })
        .then(responseJSON => onSuccess(responseJSON))
        .catch(() => onSuccess({ failed: true }));
      return true;
    }
  }
);

let defaultSuggestion = {};

function setDefaultSuggestion(suggestion) {
  defaultSuggestion = suggestion;
  chrome.omnibox.setDefaultSuggestion({ description: suggestion.description });
}

function removeDefaultSuggestion() {
  chrome.omnibox.setDefaultSuggestion({ description: "Sorry, nothing found ☹️" });
  defaultSuggestion = {};
}

function nameIncludesSearchTerm(name, searchTerm) {
  return name.toLowerCase().includes(searchTerm.toLowerCase());
}

function urlIncludesSearchTerm(url, searchTerm) {
  return url.toLowerCase().includes(searchTerm.toLowerCase());
}

function singleURLBookmarkIncludesSearchTerm(singleURLBookmarkItem, searchTerm) {
  return nameIncludesSearchTerm(singleURLBookmarkItem.name, searchTerm) || urlIncludesSearchTerm(singleURLBookmarkItem.url, searchTerm);
}

function multiURLBookmarkIncludesSearchTerm(multiURLItem, searchTerm) {
  return nameIncludesSearchTerm(multiURLItem.name, searchTerm) || urlIncludesSearchTerm(multiURLItem.url, searchTerm);
}

function isMultiURLItem(bookmark) {
  return bookmark.childrenType && bookmark.childrenType === "multiURL";
}

function addItemToSuggestion(suggestions, content, description) {
  suggestions.push({ content, description: xmlescape(description) });
};

function search(searchTerm, suggest) {
  return chrome.storage.local.get("bookmarks", (bookmarks) => {
    const foundItems = [];
    bookmarks.bookmarks.bookmarks.forEach(category => {
      category.children.forEach(bookmark => {
        if (isMultiURLItem(bookmark)) {
          bookmark.children.forEach(multiUrlItem => {
            if (nameIncludesSearchTerm(bookmark.name, searchTerm) || multiURLBookmarkIncludesSearchTerm(multiUrlItem, searchTerm)) {
              addItemToSuggestion(foundItems, multiUrlItem.url, `${multiUrlItem.name} → ${bookmark.name} (${category.name})`);
            }
          });
        } else {
          if (singleURLBookmarkIncludesSearchTerm(bookmark, searchTerm)) {
            addItemToSuggestion(foundItems, bookmark.url, `${bookmark.name} (${category.name})`);
          }
        }
      });
    });
    if (foundItems.length !== 0) {
      setDefaultSuggestion(foundItems.shift());
    } else {
      removeDefaultSuggestion();
    }
    suggest(foundItems);
  });
}

chrome.omnibox.onInputChanged.addListener((text, suggest) => {
  return search(text, suggest);
});

chrome.omnibox.onInputEntered.addListener((url) => {
  if (url.startsWith("http://") || url.startsWith("https://")) {
    chrome.tabs.update({
      url: url
    });
  } else {
    chrome.tabs.update({
      url: defaultSuggestion.content
    });
  }
});
