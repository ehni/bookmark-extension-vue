import Vue from "vue";
import Vuex from "vuex";
import router from "@/popup/router";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    bookmarks: null,
    keyListener() {
      router.push("Search");
    }
  },
  getters: {
    customTitle: state => {
      return state.bookmarks.customTitle;
    },
    customIcon: state => {
      return state.bookmarks.customIcon;
    },
    bookmarksArray: state => {
      return state.bookmarks.bookmarks;
    },
    recentlyBookmarks: state => {
      return state.bookmarks.bookmarks[0];
    }
  },
  mutations: {
    addKeyListener(state) {
      window.addEventListener("keypress", state.keyListener);
    },
    removeKeyListener(state) {
      window.removeEventListener("keypress", state.keyListener);
    },
    showNotification(state, opts) {
      this._vm.$fdNotifications.hideAll();
      const notificationOptions = {
        dismissible: true,
        permanent: false,
        type: "success",
        ...opts
      };
      this._vm.$fdNotifications.show(notificationOptions);
    },
    createCategory(state, categoryName) {
      const newCategory = { name: categoryName, type: "folder", children: [] };
      state.bookmarks.bookmarks.push(newCategory);
    },
    deleteCategory(state, categoryName) {
      state.bookmarks.bookmarks = state.bookmarks.bookmarks.filter(category => {
        return category.name !== categoryName;
      });
    },
    addBookmark(state, payload) {
      state.bookmarks.bookmarks.some(category => {
        if (category.name === payload.categoryName) {
          category.children.push(payload.newBookmark);
          return true;
        }
      });
    },
    updateBookmark(state, payload) {
      state.bookmarks.bookmarks.some(category => {
        if (category.name === payload.categoryName) {
          category.children.some((bookmark, index) => {
            if (bookmark.name === payload.oldName) {
              category.children.splice(index, 1, payload.updatedBookmark);
              return true;
            }
          });
          return true;
        }
      });
    },
    deleteBookmark(state, payload) {
      state.bookmarks.bookmarks.some(category => {
        if (category.name === payload.categoryName) {
          category.children.some((bookmark, index) => {
            if (bookmark.name === payload.name) {
              category.children.splice(index, 1);
              return true;
            }
          });
          return true;
        }
      });
    },
    setBookmarks(state, bookmarks) {
      const bookmarksCopy = JSON.parse(JSON.stringify(bookmarks));
      state.bookmarks = bookmarksCopy;
      chrome.storage.local.set({ bookmarksCopy }, () => {});
    },
    writeBookmarksToStorage(state) {
      const bookmarks = JSON.parse(JSON.stringify(state.bookmarks));
      chrome.storage.local.set({ bookmarks }, () => {});
    },
    resetBookmarks(state) {
      state.bookmarks = null;
      chrome.storage.local.clear();
    },
    exportBookmarksToJson(state) {
      chrome.storage.local.get("bookmarks", function(result) {
        if (result.bookmarks) {
          const bookmarksWithoutRecently = result.bookmarks;
          bookmarksWithoutRecently.bookmarks.shift();
          const formattedData = "data:application/json;charset=utf-8," + encodeURIComponent(JSON.stringify(bookmarksWithoutRecently));
          const fileName = "bookmarks.json";
          const linkElement = document.createElement("a");
          linkElement.setAttribute("href", formattedData);
          linkElement.setAttribute("download", fileName);
          linkElement.click();
        } else {
        }
      });
    }
  },
  actions: {
    loadBookmarksFromStorage(context) {
      chrome.storage.local.get("bookmarks", function(result) {
        if (result.bookmarks && Object.keys(result.bookmarks).length !== 0) {
          context.commit("setBookmarks", result.bookmarks);
          router.push("/");
        } else if (!result.bookmark && browser.storage.local.get("bookmarks")) {
          // Firefox compatibility
          const stuff = browser.storage.local.get("bookmarks");
          if (Object.keys(stuff).length !== 0) {
            context.commit("setBookmarks", JSON.parse(stuff));
            router.push("/");
          }
        }
      });
    },
    loadBookmarks(context, bookmarks) {
      setTimeout(() => {
        const recentlyCategory = { name: "Recently", type: "folder", children: [] };
        bookmarks.bookmarks.unshift(recentlyCategory);
        context.commit("setBookmarks", bookmarks);
        context.commit("writeBookmarksToStorage");
        context.commit("showNotification", {
          content: "Bookmarks loaded"
        });
      }, 600);
    },
    exportBookmarksToJson(context) {
      context.commit("exportBookmarksToJson");
      context.commit("showNotification", {
        content: "Bookmarks exported"
      });
    },
    createCategory(context, categoryName) {
      context.commit("createCategory", categoryName);
      context.commit("writeBookmarksToStorage");
      context.commit("showNotification", {
        content: `Category "${categoryName}" created.`
      });
    },
    deleteCategory(context, categoryName) {
      context.commit("deleteCategory", categoryName);
      context.commit("writeBookmarksToStorage");
      context.commit("showNotification", {
        content: `Category "${categoryName}" deleted.`
      });
    },
    addBookmark(context, payload) {
      const newBookmark = {
        name: payload.name
      };
      if (payload.children.length === 1) {
        newBookmark.url = payload.children[0].url;
      } else {
        newBookmark.childrenType = "multiURL";
        newBookmark.children = payload.children;
      }
      context.commit("addBookmark", { categoryName: payload.category, newBookmark });
      context.commit("writeBookmarksToStorage");
      context.commit("showNotification", {
        content: "Bookmark created."
      });
    },
    updateBookmark(context, payload) {
      const updatedBookmark = {
        name: payload.name
      };
      if (payload.children.length === 1) {
        updatedBookmark.url = payload.children[0].url;
      } else {
        updatedBookmark.childrenType = "multiURL";
        updatedBookmark.children = payload.children;
      }
      context.commit("updateBookmark", { categoryName: payload.category, oldName: payload.oldName, updatedBookmark });
      context.commit("writeBookmarksToStorage");
      context.commit("showNotification", {
        content: "Bookmark updated."
      });
    },
    deleteBookmark(context, payload) {
      context.commit("deleteBookmark", { categoryName: payload.category, name: payload.name });
      context.commit("writeBookmarksToStorage");
      context.commit("showNotification", {
        content: "Bookmark deleted."
      });
    },
    resetBookmarks(context) {
      context.commit("resetBookmarks");
      context.commit("showNotification", {
        content: "Bookmarks resetted"
      });
      router.push("/setup");
    }
  },
  modules: {
  }
});
