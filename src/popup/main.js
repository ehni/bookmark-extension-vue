import Vue from "vue";
import Vuex from "vuex";
import store from "@/popup/store";
import router from "@/popup/router";
import App from "@/popup/App.vue";
import FundamentalVue from "fundamental-vue";

Vue.use(FundamentalVue);
Vue.use(Vuex);

/* eslint-disable no-new */
new Vue({
  store,
  router,
  el: "#app",
  render: h => h(App),
  beforeCreate() {
    this.$store.dispatch("loadBookmarksFromStorage");
  }
});
