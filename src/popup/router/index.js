import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/popup/views/Home.vue";
import store from "@/popup/store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/search",
    name: "Search",
    component: () => import(/* webpackChunkName: "about" */ "@/popup/views/Search.vue")
  },
  {
    path: "/setup",
    name: "Setup",
    component: () => import(/* webpackChunkName: "about" */ "@/popup/views/Setup.vue")
  }
];

const router = new VueRouter({ routes });

router.beforeEach((to, from, next) => {
  if (to.name === "Home" && store.state.bookmarks === null) {
    next({ name: "Setup" });
  } else {
    next();
  }
});

export default router;
