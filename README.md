# Bookmark Extension

→ Download Chrome: [Team Bookmarks](https://chrome.google.com/webstore/detail/team-bookmarks/iajngfnciimpmongkmmhoplkejjloini)

→ Download Firefox: [Team Bookmarks](https://addons.mozilla.org/de/firefox/addon/team-bookmarks/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)

* Chrome extension which offers quick access to frequently used bookmarks.
* Built using Vue.js & Fundamental-Vue

![Bookmarks Overview](screenshot_bookmarks_overview.png)

## Features

* Export or import bookmarks from JSON
* Load bookmarks from online source
* Add, edit and delete cookmarks & categories 
* Search for bookmarks
* Directly integrated into Chrome Omnibar (Address bar):
    * Type `bm` and start searching for bookmarks from the extension!
* Open bookmarks in current or new tab
* Open the extension with hotkeys:
    * default: `CTRL`+`SHIFT`+`E`
    * mac: `CMD`+`SHIFT`+`E`

## Known issues / TODOs

* Rename categories currently not supported
* Not tested
* Clean up code

## How to install

### From webstore

- [Chrome](https://chrome.google.com/webstore/detail/team-bookmarks/iajngfnciimpmongkmmhoplkejjloini)
- [Firefox](https://addons.mozilla.org/de/firefox/addon/team-bookmarks/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
### Directly from this repository

1. Clone the repository
1. Run `npm run install` to install depedencies
1. Run `npm run serve` to build locally. After a successfull build you can just run `CTRL`+`C` to shut down the local server.
1. Open Chrome extension settings either 
	* navigating to [chrome://extensions/](chrome://extensions/) or
	* going to `Settings` -> `More Tools`-> `Extensions`.
1. Activate `Developer Mode` in the right hand corner.
1. Click `load unpacked extension` and load extension from `<repository>/dist`

## Create your own bookmarks for the extension

The extension reads all the bookmarks from a json file and saves them in the localstorage afterwards.
You can easily create your own customized bookmarks extension by providing a json file.

Here's a quick overview of the bookmarks.json and how you can set up your own bookmarks:

```
{
  "customTitle": "Title for the extension",
  "customIcon": "database",
  "bookmarks": [
	{
	  "name": "CategoryName",
	  "children": [
	  ]
	}
  ]
}
```

You can use any icon of your choice from [Fundamental-Vue](https://sap.github.io/fundamental-vue/#/examples/icon)

You can add your own bookmarks to the `bookmarks` array in the following types:

* New category:

```
{
  "name": "Category",
  "children": [
  	...
  ]
}
```

Each category can contain bookmarks. There are two types of bookmarks:

* Single url bookmarks

```
{
  "name": "Team Wiki",
  "url": "<team wiki url>"
},
```

* Multi url bookmarks

```
{
  "name": "Lunch Menu",
  "childrenType": "multiURL",
  "children": [
	{
	  "name": "Canteen A",
	  "url": "<url lunch menu canteen a>"
	},
    {
	  "name": "Canteen B",
	  "url": "<url lunch menu canteen b>"
	},
	...
  ]
}
```

## Contribute

Feel free to send pull requests  🎉

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Lints and fixes files
```
npm run lint
```
