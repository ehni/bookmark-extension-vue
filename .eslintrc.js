module.exports = {
  root: true,
  env: {
    node: true,
    webextensions: true
  },
  extends: [
    "plugin:vue/essential",
    "@vue/standard"
  ],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    quotes: [2, "double", { avoidEscape: true }],
    semi: ["error", "always"],
    "space-before-function-paren": ["error", "never"],
    "vue/multi-word-component-names": 0
  }
};
